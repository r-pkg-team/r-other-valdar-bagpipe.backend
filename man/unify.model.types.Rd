\name{unify.model.types}
\alias{unify.model.types}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{unify.model.types
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
unify.model.types()
}
%- maybe also 'usage' for other objects documented here.
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function () 
{
    synonyms <- list(binary = "binomial", binomial = "binomial", 
        coxph = "coxph", Gamma = "Gamma", gamma = "Gamma", gaussian = "gaussian", 
        linear = "gaussian", negative.binomial = "negative.binomial", 
        negbin = "negative.binomial", ordinal = "polr", overdispersed.poisson = "negative.binomial", 
        poisson = "poisson", polr = "polr", quasipoisson = "quasipoisson", 
        survival = "survreg", survreg = "survreg")
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
