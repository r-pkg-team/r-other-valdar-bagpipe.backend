\name{permutation.matrix}
\alias{permutation.matrix}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{permutation.matrix
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
permutation.matrix(num.perms, data, column = NULL, seed = NULL, rows = 1:nrow(data))
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{num.perms}{
%%     ~~Describe \code{num.perms} here~~
}
  \item{data}{
%%     ~~Describe \code{data} here~~
}
  \item{column}{
%%     ~~Describe \code{column} here~~
}
  \item{seed}{
%%     ~~Describe \code{seed} here~~
}
  \item{rows}{
%%     ~~Describe \code{rows} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (num.perms, data, column = NULL, seed = NULL, rows = 1:nrow(data)) 
{
    if (!is.null(seed)) 
        set.seed(seed)
    if (!is.null(column) & num.perms > 0) {
        mat <- matrix(nrow = nrow(data), ncol = num.perms)
        id <- c(1:nrow(data))
        sp <- split(id, as.factor(column))
        for (j in 1:num.perms) {
            mat[, j] <- id
            for (f in names(sp)) {
                if (length(sp[[f]]) > 1) {
                  mat[sp[[f]], j] <- sample(sp[[f]], replace = F)
                }
            }
        }
        return(mat)
    }
    else if (num.perms > 0) {
        cat("permuting randomly with no cols\n")
        mat <- matrix(nrow = nrow(data), ncol = num.perms)
        id <- 1:nrow(data)
        permutable <- id \%in\% rows
        for (j in 1:num.perms) {
            mat[permutable, j] <- sample(rows, replace = F)
            mat[!permutable, j] <- id[!permutable]
        }
        return(mat)
    }
    return(NULL)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
