\name{happy.get.interval.over}
\alias{happy.get.interval.over}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{happy.get.interval.over
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
happy.get.interval.over(h, chromosome, x, scale = "cM", use.nearest.terminus = FALSE, boundary.choice = "l", fudge.bp = FALSE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{h}{
%%     ~~Describe \code{h} here~~
}
  \item{chromosome}{
%%     ~~Describe \code{chromosome} here~~
}
  \item{x}{
%%     ~~Describe \code{x} here~~
}
  \item{scale}{
%%     ~~Describe \code{scale} here~~
}
  \item{use.nearest.terminus}{
%%     ~~Describe \code{use.nearest.terminus} here~~
}
  \item{boundary.choice}{
%%     ~~Describe \code{boundary.choice} here~~
}
  \item{fudge.bp}{
%%     ~~Describe \code{fudge.bp} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (h, chromosome, x, scale = "cM", use.nearest.terminus = FALSE, 
    boundary.choice = "l", fudge.bp = FALSE) 
{
    chromosome <- rep(chromosome, length.out = length(x))
    boundary.choice <- rep(boundary.choice, length.out = length(x))
    markers <- happy.get.markers(h, chromosome = chromosome)
    chrom <- happy.get.chromosome(h, markers)
    range <- happy.get.interval.range(h, markers, scale = scale, 
        fudge.bp = fudge.bp)
    overlap.marker <- rep(NA, length(x))
    for (i in 1:length(x)) {
        my.chrom <- chromosome[i]
        my.loc <- x[i]
        overlap.idx <- which(my.chrom == chrom & my.loc >= range[, 
            1] & my.loc <= range[, 2])
        if (2 == length(overlap.idx)) {
            if (is.na(boundary.choice[i])) {
                overlap.marker[i] <- NA
            }
            else if ("l" == boundary.choice[i]) {
                overlap.marker[i] <- markers[overlap.idx[1]]
            }
            else {
                overlap.marker[i] <- markers[overlap.idx[2]]
            }
        }
        if (1 == length(overlap.idx)) {
            overlap.marker[i] <- markers[overlap.idx]
        }
        if (0 == length(overlap.idx) & use.nearest.terminus) {
            is.early <- my.loc < happy.get.location(h, scale = scale, 
                happy.get.first.marker(h, chromosome = my.chrom))
            overlap.marker[i] <- ifelse(is.early, happy.get.first.marker(h, 
                chromosome = my.chrom), happy.get.last.marker(h, 
                chromosome = my.chrom))
        }
    }
    overlap.marker
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
