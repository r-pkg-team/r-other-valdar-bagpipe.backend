\name{is.formula}
\alias{is.formula}
\title{Tests for objects of type \code{formula}.
}
\description{
Provides a test of whether an object inherits from class \code{formula}.
}
\usage{
is.formula(x)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{x}{
	Object to be tested.
}
}
\details{
	A wrapper for \code{inherits(x, "formula")}.
}
\value{
	A logical scalar equal to TRUE if x inherits from \code{formula} and FALSE otherwise.
}
\author{
William Valdar <william.valdar@unc.edu>
}
\seealso{
	\code{as.integer}
}
\examples{
	is.formula(y ~ a + bx)
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
