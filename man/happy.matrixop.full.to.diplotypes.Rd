\name{happy.matrixop.full.to.diplotypes}
\alias{happy.matrixop.full.to.diplotypes}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{happy.matrixop.full.to.diplotypes
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
happy.matrixop.full.to.diplotypes(x, num.strains)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{x}{
%%     ~~Describe \code{x} here~~
}
  \item{num.strains}{
%%     ~~Describe \code{num.strains} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (x, num.strains) 
{
    m <- matrix(0, ncol = num.strains, nrow = num.strains)
    diag(m) <- x[1:num.strains]
    m[upper.tri(m, diag = FALSE)] = x[(num.strains + 1):length(x)]
    0.5 * (m + t(m))
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
