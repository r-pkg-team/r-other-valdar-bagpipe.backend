\name{configfile.string}
\alias{configfile.string}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{Get character string field
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
configfile.string(config, keys, default = NULL, stop.on.fail = TRUE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{config}{
%%     ~~Describe \code{config} here~~
}
  \item{keys}{
%%     ~~Describe \code{keys} here~~
}
  \item{default}{
%%     ~~Describe \code{default} here~~
}
  \item{stop.on.fail}{
%%     ~~Describe \code{stop.on.fail} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
William Valdar <william.valdar@unc.edu>
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (config, keys, default = NULL, stop.on.fail = TRUE) 
{
    if (!mdlist.has(config, keys)) {
        if (!stop.on.fail | (missing(stop.on.fail) & !missing(default))) {
            return(default)
        }
        stop("Cannot find parameters ", keys, " in configfile\n")
    }
    return(mdlist.get(config, keys))
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
