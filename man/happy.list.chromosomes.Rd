\name{happy.list.chromosomes}
\alias{happy.list.chromosomes}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{happy.list.chromosomes
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
happy.list.chromosomes(h, sort = TRUE, model = "genotype")
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{h}{
%%     ~~Describe \code{h} here~~
}
  \item{sort}{
%%     ~~Describe \code{sort} here~~
}
  \item{model}{
%%     ~~Describe \code{model} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (h, sort = TRUE, model = "genotype") 
{
    assert.happy(h)
    chr <- unique(as.character(h[[model]]$genome$chromosome))
    if (sort) {
        ints <- suppressWarnings(as.integer(chr))
        chars <- chr[is.na(ints)]
        ints <- ints[!is.na(ints)]
        chr <- c(as.character(sort(ints)), sort(chars))
    }
    chr
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
