\name{DEFAULT.REDUCE.DMAT.CUTOFF}
\alias{DEFAULT.REDUCE.DMAT.CUTOFF}
\docType{data}
\title{DEFAULT.REDUCE.DMAT.CUTOFF
%%   ~~ data name/kind ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of the dataset. ~~
}
\usage{data(DEFAULT.REDUCE.DMAT.CUTOFF)}
\format{
  The format is:
 num 0.01
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
}
\references{
%%  ~~ possibly secondary sources and usages ~~
}
\examples{
data(DEFAULT.REDUCE.DMAT.CUTOFF)
## maybe str(DEFAULT.REDUCE.DMAT.CUTOFF) ; plot(DEFAULT.REDUCE.DMAT.CUTOFF) ...
}
\keyword{datasets}
