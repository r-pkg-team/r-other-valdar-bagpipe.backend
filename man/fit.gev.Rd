\name{fit.gev}
\alias{fit.gev}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{fit.gev
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
fit.gev(data, thresholds)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{data}{
%%     ~~Describe \code{data} here~~
}
  \item{thresholds}{
%%     ~~Describe \code{thresholds} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (data, thresholds) 
{
    require(evd)
    model.gev <- fgev(data)
    gev.df <- data.frame(loc = model.gev$estimate[1], loc.se = model.gev$std.err[1], 
        scale = model.gev$estimate[2], scale.se = model.gev$std.err[2], 
        shape = model.gev$estimate[3], shape.se = model.gev$estimate[3])
    gev.thresholds.df <- data.frame(upper.tail.prob = thresholds, 
        quantile = qgev(thresholds, loc = gev.df$loc, scale = gev.df$scale, 
            shape = gev.df$shape, lower.tail = FALSE))
    return(list(thresholds = gev.thresholds.df, gev = gev.df))
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
