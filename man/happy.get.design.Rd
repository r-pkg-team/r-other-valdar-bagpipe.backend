\name{happy.get.design}
\alias{happy.get.design}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{happy.get.design
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
happy.get.design(h, marker, model = "additive", subjects = NULL, as.data.frame = TRUE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{h}{
%%     ~~Describe \code{h} here~~
}
  \item{marker}{
%%     ~~Describe \code{marker} here~~
}
  \item{model}{
%%     ~~Describe \code{model} here~~
}
  \item{subjects}{
%%     ~~Describe \code{subjects} here~~
}
  \item{as.data.frame}{
%%     ~~Describe \code{as.data.frame} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (h, marker, model = "additive", subjects = NULL, as.data.frame = TRUE) 
{
    assert.happy(h)
    if ("genotype" == model) {
        model <- "genotype.full"
    }
    hmodel <- model
    submodel <- NULL
    if (igrep("genotype", model)) {
        submodel <- sub("genotype.", "", model)
        hmodel <- "genotype"
    }
    if ("dominance" == model) {
        hmodel <- "full"
    }
    mat <- happy.load.marker(h, marker = marker, model = hmodel)
    if ("genotype" == hmodel) {
        if (submodel \%in\% c("additive", "dominance", "hier")) {
            mat <- as.matrix(genotype.to.hier(as.vector(mat)))
            if ("additive" == submodel) {
                mat <- mat[, 1]
            }
            else if ("dominance" == submodel) {
                mat <- mat[, 2]
            }
        }
        else if ("full" == submodel) {
            mat <- as.factor(mat)
        }
        else if ("ped" == submodel) {
            g <- genotype.to.count(as.vector(mat))
            mat <- rep("00", length(g))
            mat[g == 0] <- 11
            mat[g == 1] <- 12
            mat[g == 2] <- 22
        }
        else {
            stop("Unknown model: ", model, "\n")
        }
    }
    if (is.null(dim(mat))) {
        mat <- as.array(mat)
    }
    if (!is.null(subjects)) {
        subjects <- as.character(subjects)
        i <- match(subjects, happy.get.subjects(h))
        if (1 == length(dim(mat))) {
            mat <- mat[i]
        }
        else {
            mat <- matrix(mat[i, ], nrow = length(subjects), 
                ncol = ncol(mat), dimnames = list(subjects, colnames(mat)))
        }
    }
    if ("dominance" == model) {
        mat <- mat[, -(1:length(happy.get.strains(h)))]
    }
    if (as.data.frame) {
        mat <- as.data.frame(mat)
        if (1 == ncol(mat)) {
            colnames(mat) <- model
        }
        if (!is.null(subjects)) {
            rownames(mat) <- subjects
        }
    }
    mat
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
