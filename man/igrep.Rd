\name{igrep}
\alias{igrep}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{Wrapper for \code{grep} providing a logical vector output
}
\description{
A pass-through method for \code{grep} that provides output as a logical vector indicating which elements matched. If argument \code{logical=FALSE} then simply returns the output from standard \code{grep}. 
}
\usage{
igrep(pattern, x, ..., value = FALSE, logical = TRUE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{pattern}{
	As for the standard function \code{grep}.
}
  \item{x}{
%%     ~~Describe \code{x} here~~
}
  \item{\dots}{
	Arguments to \code{grep}
}
  \item{value}{
%%     ~~Describe \code{value} here~~
}
  \item{logical}{
%%     ~~Describe \code{logical} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (pattern, x, ..., value = FALSE, logical = TRUE) 
{
    if (!value & logical) {
        indices <- grep(pattern, x, value = value, ...)
        return(1:length(x) \%in\% indices)
    }
    grep(pattern, x, value = value, ...)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
