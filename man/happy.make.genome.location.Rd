\name{happy.make.genome.location}
\alias{happy.make.genome.location}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{happy.make.genome.location
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
happy.make.genome.location(h, markers = NULL, chr = NULL, x = NULL, pretty = TRUE, scale = "cM", pad = switch(scale, cM = ifelse(pretty, 10, 0), bp = ifelse(pretty, 2e+07, 0)))
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{h}{
%%     ~~Describe \code{h} here~~
}
  \item{markers}{
%%     ~~Describe \code{markers} here~~
}
  \item{chr}{
%%     ~~Describe \code{chr} here~~
}
  \item{x}{
%%     ~~Describe \code{x} here~~
}
  \item{pretty}{
%%     ~~Describe \code{pretty} here~~
}
  \item{scale}{
%%     ~~Describe \code{scale} here~~
}
  \item{pad}{
%%     ~~Describe \code{pad} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (h, markers = NULL, chr = NULL, x = NULL, pretty = TRUE, 
    scale = "cM", pad = switch(scale, cM = ifelse(pretty, 10, 
        0), bp = ifelse(pretty, 2e+07, 0))) 
{
    chroms <- happy.list.chromosomes(h)
    first.markers <- happy.get.first.marker(h, chroms)
    chrom.offset <- rep(0, length(chroms))
    chrom.offset[1] <- happy.get.location(h, first.markers[1], 
        scale = scale)
    for (i in 2:length(chroms)) {
        prev.chr.length <- happy.get.chromosome.length(h, chroms[i - 
            1], scale = scale, subtract.offset = FALSE)
        offset <- happy.get.location(h, first.markers[i], scale = scale)
        chrom.offset[i] <- chrom.offset[i - 1] + prev.chr.length - 
            offset + pad
    }
    chrmap = data.frame(chr = I(chroms), offset = chrom.offset)
    chrmap$start <- chrom.offset + happy.get.location(h, first.markers, 
        scale = scale)
    chrmap$end <- chrom.offset + happy.get.chromosome.length(h, 
        chroms, scale = scale)
    chrmap$midpoint <- 0.5 * (chrmap$start + chrmap$end)
    retval <- list(chr.limits = chrmap)
    if (!is.null(x) & !is.null(chr)) {
        stopifnot(length(x) == length(chr))
        x <- x + chrmap$offset[match(chr, chrmap$chr)]
        retval$x <- x
    }
    if (!is.null(markers)) {
        marker.chr <- happy.get.chromosome(h, markers)
        i <- match(marker.chr, chrmap$chr)
        z <- happy.get.location(h, markers, scale = scale)
        z <- z + chrmap$offset[i]
        retval$marker <- markers
        retval$genome.location <- z
        retval$scale <- scale
    }
    retval
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
