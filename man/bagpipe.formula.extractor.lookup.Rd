\name{bagpipe.formula.extractor.lookup}
\alias{bagpipe.formula.extractor.lookup}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{bagpipe.formula.extractor.lookup
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
bagpipe.formula.extractor.lookup(extractor = NULL, model = NULL)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{extractor}{
%%     ~~Describe \code{extractor} here~~
}
  \item{model}{
%%     ~~Describe \code{model} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (extractor = NULL, model = NULL) 
{
    extractor.info <- list(interval.additive = list(tag = "additive", 
        happy.model = "additive"), interval.full = list(tag = "full", 
        happy.model = "full"), interval.dominance = list(tag = "dominance", 
        happy.model = "dominance"), interval.prob.hom = list(tag = "prob.hom", 
        happy.model = NA), genotype = list(tag = "genotype", 
        happy.model = "genotype"), genotype.additive = list(tag = "genotype.additive", 
        happy.model = "genotype.additive"), genotype.hier = list(tag = "genotype.hier", 
        happy.model = "genotype.hier"), interval.fullasym = list(tag = "fullasym", 
        happy.model = "fullasym"))
    if (!is.null(extractor)) {
        return(extractor.info[[extractor, exact = TRUE]])
    }
    extractor.table = data.frame(extractor = names(extractor.info), 
        tag = sapply(extractor.info, function(x) {
            x$tag
        }), happy.model = sapply(extractor.info, function(x) {
            x$happy.model
        }), stringsAsFactors = FALSE)
    if (!is.null(model)) {
        return(extractor.table$extractor[match(model, extractor.table$happy.model)])
    }
    extractor.table
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
