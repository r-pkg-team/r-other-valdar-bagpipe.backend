\name{happy.plot.ladder.chr.list}
\alias{happy.plot.ladder.chr.list}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{happy.plot.ladder.chr.list
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
happy.plot.ladder.chr.list(chr.names, data, chr.col = "chr", wanted.cols = "locus", constant.cols = list())
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{chr.names}{
%%     ~~Describe \code{chr.names} here~~
}
  \item{data}{
%%     ~~Describe \code{data} here~~
}
  \item{chr.col}{
%%     ~~Describe \code{chr.col} here~~
}
  \item{wanted.cols}{
%%     ~~Describe \code{wanted.cols} here~~
}
  \item{constant.cols}{
%%     ~~Describe \code{constant.cols} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (chr.names, data, chr.col = "chr", wanted.cols = "locus", 
    constant.cols = list()) 
{
    chr.list <- list()
    for (chr in chr.names) {
        i <- data[, chr.col] == as.character(chr)
        d <- data[i, wanted.cols]
        for (nam in names(constant.cols)) {
            d[, nam] <- constant.cols[[nam]]
        }
        chr.list[[chr]] <- d
    }
    chr.list
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
