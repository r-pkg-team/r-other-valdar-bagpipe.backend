\name{happy.check.bp}
\alias{happy.check.bp}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{happy.check.bp
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
happy.check.bp(h, stop.on.fail = TRUE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{h}{
%%     ~~Describe \code{h} here~~
}
  \item{stop.on.fail}{
%%     ~~Describe \code{stop.on.fail} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (h, stop.on.fail = TRUE) 
{
    WARN <- stop
    if (!stop.on.fail) 
        WARN <- warning
    assert.happy(h)
    ok <- TRUE
    for (chr in happy.list.chromosomes(h)) {
        pos <- happy.get.position(h, happy.get.markers(h, chromosome = chr))
        if (any(order(pos) != 1:length(pos))) {
            ok <- FALSE
            WARN("Disorder in internal representation: ", "markers are not in cM order\n")
        }
    }
    all.markers <- happy.get.markers(h)
    if (any(is.na(happy.get.bp(h, all.markers)))) {
        ok <- FALSE
        WARN("Some markers with NA bp\n")
    }
    ok
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
