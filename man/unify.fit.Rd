\name{unify.fit}
\alias{unify.fit}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{unify.fit
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
unify.fit(formula, data, model.type = "linear", args = list())
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{formula}{
%%     ~~Describe \code{formula} here~~
}
  \item{data}{
%%     ~~Describe \code{data} here~~
}
  \item{model.type}{
%%     ~~Describe \code{model.type} here~~
}
  \item{args}{
%%     ~~Describe \code{args} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (formula, data, model.type = "linear", args = list()) 
{
    form <- as.formula(formula)
    is.multilevel <- 0 < length(grep(pattern = "\\|", formula.as.string(form)))
    type <- unify.model.types()[[model.type]]
    if (is.null(type)) {
        stop("Cannot currently fit models of type ", model.type, 
            "\n")
    }
    fit <- NULL
    if (!is.multilevel) {
        args$formula <- formula
        args$data <- quote(data)
        if ("gaussian" == type) {
            fit <- do.call("lm", args = args)
        }
        else if (type \%in\% c("binomial", "Gamma", "poisson", 
            "quasipoisson")) {
            args$family <- type
            fit <- do.call("glm", args = args)
        }
        else if ("negative.binomial" == type) {
            fit <- do.call("glm.nb", args = args)
        }
        else if ("survreg" == type) {
            fit <- do.call("survreg", args = args)
        }
        else if ("coxph" == type) {
            fit <- do.call("coxph", args = args)
        }
        else if ("polr" == type) {
            fit <- do.call("polr", args = args)
        }
        else {
            stop("Cannot fit unilevel model.type ", model.type, 
                "\n")
        }
    }
    else {
        require(lme4)
        args$formula <- formula
        args$data <- quote(data)
        if ("gaussian" == type) {
            fit <- do.call("lmer", args = args)
        }
        else if (type \%in\% c("binomial", "Gamma", "poisson", 
            "quasipoisson")) {
            args$family <- type
            fit <- do.call("lmer", args = args)
        }
        else {
            stop("Cannot fit multilevel model.type ", model.type, 
                "\n")
        }
    }
    fit
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
