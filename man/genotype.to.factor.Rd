\name{genotype.to.factor}
\alias{genotype.to.factor}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{genotype.to.factor
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
genotype.to.factor(g)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{g}{
%%     ~~Describe \code{g} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (g) 
{
    unphased <- rep(NA, length = nrow(g))
    ok <- complete.cases(g)
    ordered <- g[, 1] <= g[, 2]
    mask <- ordered & ok
    unphased[mask] <- paste(g[mask, 1], g[mask, 2], sep = "_")
    mask <- !ordered & ok
    unphased[mask] <- paste(g[mask, 1], g[mask, 2], sep = "_")
    as.factor(unphased)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
