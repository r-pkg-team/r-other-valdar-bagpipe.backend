\name{general.scan}
\alias{general.scan}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{general.scan
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
general.scan(h, null.formula, test.formula, markers, data, model.type, model.args = list(), verbose = TRUE, boot.set = NULL, reduce.dmat = NULL)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{h}{
%%     ~~Describe \code{h} here~~
}
  \item{null.formula}{
%%     ~~Describe \code{null.formula} here~~
}
  \item{test.formula}{
%%     ~~Describe \code{test.formula} here~~
}
  \item{markers}{
%%     ~~Describe \code{markers} here~~
}
  \item{data}{
%%     ~~Describe \code{data} here~~
}
  \item{model.type}{
%%     ~~Describe \code{model.type} here~~
}
  \item{model.args}{
%%     ~~Describe \code{model.args} here~~
}
  \item{verbose}{
%%     ~~Describe \code{verbose} here~~
}
  \item{boot.set}{
%%     ~~Describe \code{boot.set} here~~
}
  \item{reduce.dmat}{
%%     ~~Describe \code{reduce.dmat} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (h, null.formula, test.formula, markers, data, model.type, 
    model.args = list(), verbose = TRUE, boot.set = NULL, reduce.dmat = NULL) 
{
    if (0 != length(grep("THE.LOCUS", null.formula))) {
        constant.null.model = FALSE
    }
    if (!is.null(boot.set)) {
        data <- data[boot.set, ]
    }
    num.loci <- length(markers)
    results <- data.frame(locus = I(markers), chr = happy.get.chromosome(h, 
        markers), cM = happy.get.location(h, markers, scale = "cM"), 
        bp = happy.get.location(h, markers, scale = "bp"), num.obs = rep(nrow(data), 
            num.loci), null.logLik = rep(NA, num.loci), null.num.params = rep(NA, 
            num.loci), test.logLik = rep(NA, num.loci), test.num.params = rep(NA, 
            num.loci), LOD = rep(NA, num.loci), modelcmp = rep(NA, 
            num.loci), comments = rep(NA, num.loci))
    if (verbose) {
        cat("general.scan() of", num.loci, "markers:")
    }
    for (i in 1:num.loci) {
        marker <- markers[i]
        if (verbose) {
            cat("[", i, "]", sep = "")
        }
        expanded <- happy.expand.formula(h, formulae = c(null.formula, 
            test.formula), THE.LOCUS = marker, subjects = data$SUBJECT.NAME, 
            dmat.transform.FUN = reduce.dmat)
        gdata <- cbind(data, expanded$locus.data)
        oldwarn <- options("warn")
        options(warn = 2)
        fit0 <- try(unify.fit(as.formula(expanded$formulae[1]), 
            data = gdata, model.type = model.type, args = model.args))
        options(oldwarn)
        if (caught.error(fit0)) {
            warning(paste("Warning: could not fit null model for ", 
                marker), "\n")
            next
        }
        results$null.logLik[i] <- unify.logLik(fit0)
        results$null.num.params[i] <- unify.num.params(fit0)
        options(warn = 2)
        fit1 <- try(unify.fit(as.formula(expanded$formulae[2]), 
            data = gdata, model.type = model.type, args = model.args))
        options(oldwarn)
        if (caught.error(fit1)) {
            warning(paste("Warning: could not fit null model for ", 
                marker), "\n")
            next
        }
        results$test.logLik[i] <- unify.logLik(fit1)
        results$test.num.params[i] <- unify.num.params(fit1)
        options(oldwarn)
        results$LOD[i] <- (results$test.logLik[i] - results$null.logLik[i])/log(10)
        an <- unify.anova.list(fit0, fit1)
        if (!is.finite(an$logP[2])) 
            an$logP[2] <- NA
        results$modelcmp[i] <- an$logP[2]
    }
    if (verbose) {
        cat("Done\n")
    }
    return(list(null.formula = null.formula, test.formulae = test.formula, 
        table = results, model = model.type, anova = NA, modelcmp.type = "logP"))
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
