\name{cmdline.has.option}
\alias{cmdline.has.option}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
  Check for command line arguments
}
\description{
  Returns TRUE if a key=value argument with the specified key was issued on the command line (after \code{--args}).
}
\usage{
cmdline.has.option(key)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{key}{
  The name of the command line argument.
}
}
\details{
  This command is currently implemented as a call to \code{cmdline.option} with \code{allow.omit=TRUE}, followed by a test for equality.
}
\value{
  A logical scalar that is TRUE if the specified (key=value style) argument was given on the command line and FALSE otherwise.
}
\references{
}
\author{
William Valdar <william.valdar@unc.edu>
}
\note{
}
\seealso{
  \code{cmdline.option}
}
\examples{
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
