\name{bagpipe.define.posboot.loci}
\alias{bagpipe.define.posboot.loci}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{bagpipe.define.posboot.loci
%%  ~~function to do ... ~~
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
bagpipe.define.posboot.loci(h, locus.range)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{h}{
%%     ~~Describe \code{h} here~~
}
  \item{locus.range}{
%%     ~~Describe \code{locus.range} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (h, locus.range) 
{
    if (2 != length(locus.range)) {
        bagpipe.input.error("Must specify exactly two markers for positional bootstrap")
    }
    if (!all(happy.has.markers(h, locus.range))) {
        bagpipe.input.error("Unknown markers:", paste(locus.range[!happy.has.markers(h, 
            locus.range)], collapse = ","))
    }
    if (1 != length(unique(happy.get.chromosome(h, locus.range)))) {
        bagpipe.input.error(paste(sep = "", "Cannot bootstrap between unlinked markers ", 
            locus.range[1], " (chr ", happy.get.chromosome(h, 
                locus.range[1])), ") and ", locus.range[2], " (chr ", 
            happy.get.chromosome(h, locus.range[2]), ")")
    }
    if (0 > diff(happy.get.location(h, locus.range, scale = "cM"))) {
        bagpipe.input.error("Markers for positional bootstrap must in given in chromosome order")
    }
    happy.get.markers.between(h, from = locus.range[1], to = locus.range[2])
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
