list.has <- function(x, key)
# ordinary indexing would throw an error
{
    if (is.numeric(key))
    {
        return (key <= length(x))
    }
    key %in% names(x)
}

mdlist.get <- function(obj, key)
# mdlist.*: functions for traversing a multi-dimensional list
{
    if (0==length(key))
    {
        stop("Argument \'key\' must have at least one element\n")
    }
    if (1==length(key))
    {
        return (obj[[key, exact=TRUE]])
    }
    mdlist.get(obj[[ key[1], exact=TRUE ]], key[-1])
}

mdlist.has <- function(obj, key)
# mdlist.*: functions for traversing a multi-dimensional list
{

    if (0==length(key))
    {
        stop("Argument \'key\' must have at least one element\n")
    }
    if (1==length(key))
    {
        return (list.has(obj, key))
    }
    if (1 < length(key))
    {
        if (!list.has(obj,key[1]))
        {
            return (FALSE)
        }
        return (mdlist.has(obj[[ key[1] ]], key[-1]))
    }
}


mdlist.put <- function(obj, key, value)
# mdlist.*: functions for traversing a multi-dimensional list
{
    num.keys <- length(key)
    if (0==num.keys)
    {
        stop("Argument \'key\' must have at least one element\n")
    }
    if (1==num.keys)
    {
        obj[[key]] <- value
        return (obj)
    }
    if (1 < num.keys)
    {
        if (!mdlist.has(obj, key[1]))
        {
            obj[[ key[1] ]] <- list()
        }
        obj[[ key[1] ]] <- mdlist.put(obj[[key[1]]], key[-1], value)
    }
    obj
}
