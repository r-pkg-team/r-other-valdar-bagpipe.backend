bagpipe.formula.decipher <- function(string, extractor)
{
	pattern    <- paste(
	    "[^0-9A-Za-z_\\.]", # not a variable, number or function
	    extractor,
	    sep="",
#      "\\(\\s*[0-9A-Za-z_\\.]+\\s*\\)")
      "\\([^\\)]+\\)")
	re.matches <- gregexpr(pattern, text=string, perl=TRUE)[[1]]
	
	if (re.matches[1]==-1)
	{
		return(list(chunks=list(list(before=string)), after=""))
	}
	chunks <- list()
	prev.end <- 0
	for (i in 1:length(re.matches))
	{
		re.start <- re.matches[i]
		re.end   <- re.start + attr(re.matches, "match.length")[i] - 1
		locus.cipher <- substring(string, re.start+1, re.end)
		arg.string <- string.trim(gsub("\\)", "", gsub(".*\\(", "", locus.cipher))) 
		args <- unlist(strsplit(arg.string, split="\\s*,\\s*", perl=TRUE))
    # first argument is assumed to be the locus name
		locus.name <- args[1]
		
		# subject arguments are options, and may be absent entirely
		args <- args[-1]
		arg.list=list()
		for (a in args)
		{
		  keyvalue=unlist(strsplit(split="\\s*=\\s*",a))
		  arg.list[[keyvalue[1]]] = keyvalue[2]
		}
		 
    # unique tag for this cipher/argument combination
		tag = paste(sep="-",locus.name,extractor,paste(collapse="-",args))
		chunks[[i]] <- list(
				before = substring(string, prev.end+1, re.start),
				locus.cipher = locus.cipher,
				locus.name = locus.name,
				locus.args= arg.list,
				predictor.tag=tag)
		prev.end <- re.end
	}
	list(
  		chunks=chunks,
  		tail = substring(string, prev.end+1))
} 

bagpipe.formula.encipher.locus <- function(marker, model)
{
	paste(bagpipe.formula.extractor.lookup(model=model), sep="", "(", marker, ")")
}

bagpipe.formula.extractor.lookup <- function(extractor=NULL, model=NULL)
{
	extractor.info <- list(
  		on.chrX=list(
  		    tag="on.chrX",
  		    happy.model=NA),
			genotype           = list(
			    tag="genotype",
			    happy.model="genotype"),
			genotype.additive  = list(
			    tag="genotype.additive",
			    happy.model="genotype.additive"),
			genotype.hier      = list(
			    tag="genotype.hier",
			    happy.model="genotype.hier"),
			interval.additive  = list(
			    tag="additive",
			    happy.model="additive"),
			interval.dominance = list(
			    tag="dominance",
			    happy.model="dominance"),
			interval.full      = list(
			    tag="full",
			    happy.model="full"),
			interval.fullasym  = list(
			    tag="fullasym",
			    happy.model="fullasym"),
			interval.prob.hom  = list(
			    tag="prob.hom",
			    happy.model=NA),
			interval.prob.het  = list(
			    tag="prob.het",
			    happy.model=NA)
			)

	if (!is.null(extractor))
	{
    return (extractor.info[[extractor,exact=TRUE]])
	}
  extractor.table=data.frame(
        extractor=names(extractor.info),
        tag=sapply(extractor.info, function(x){x$tag}),
        happy.model=sapply(extractor.info, function(x){x$happy.model}),
        stringsAsFactors=FALSE)
  if (!is.null(model))
  {
    return (extractor.table$extractor[match(model,extractor.table$happy.model)])
  }
  extractor.table
}

bagpipe.formula.reserved.variables <- function(h)
{
  c("THE.LOCUS", happy.get.strains(h))
}
 
bagpipe.formula.error <- function(...)
{
  stop("Bagpipe Formula Error: ", ..., "\n", call.=FALSE)
}

bagpipe.formula.has.abstract.loci <- function(x)
{
  "THE.LOCUS" %in% split.formula(x)$predictor.vars
}

bagpipe.get.design <- function(h, locus, extractor, extractor.args, extractor.tag, subjects)
{
  column.prefix=paste(locus,sep=".",extractor.tag)
  sdp=NULL
  if (list.has(extractor.args, "sdp"))
  {
    sdp=bagpipe.parse.sdp.string(h, extractor.args$sdp)
    column.prefix=paste(column.prefix, sep="", ".sdp", extractor.args$sdp)
  }

  happy.model = bagpipe.formula.extractor.lookup(extractor)$happy.model
  data=NULL

  if (!is.na(happy.model))
  {
    data=happy.get.design(h,
        marker=locus,
        subjects=subjects,
        model=happy.model,
        sdp=sdp,
        as.data.frame=FALSE)
		if (1==ncol(data))
		{
			colnames(data) <- make.names(column.prefix)
		}
		else
		{
			colnames(data) <- make.names(paste(column.prefix,sep=".",colnames(data)))
		}
	} 
  else if ("on.chrX"==extractor)
  {
    chr=happy.get.chromosome(h, marker=locus)
    data=matrix(as.numeric("X"==chr), nrow=length(subjects))
    colnames(data)=column.prefix
  } 
  else if ("interval.prob.het"==extractor)
  {
    if (length(extractor.args)!=0)
    {
      stop("Cannot currently handle arguments to interval.prob.het\n")
    }
    hd=happy.get.design(h, marker=locus, subjects=subjects,
        model="dominance", as.data.frame=FALSE)
    data=as.matrix(rowSums(hd))
    colnames(data)=column.prefix
  }
  else if ("interval.prob.hom"==extractor)
  {
    if (length(extractor.args)!=0)
    {
      stop("Cannot currently handle arguments to interval.prob.hom\n")
    }
    hd=happy.get.design(h, marker=locus, subjects=subjects,
        model="dominance", as.data.frame=FALSE)
    data=1-as.matrix(rowSums(hd))
    colnames(data)=column.prefix
  }
  else
  {
    bagpipe.formula.error(extractor, "() not implemented yet", "\n")
  }
  data
}
  


#------------------------
# happy.genome compatible
# ha == "happy all"

bagpipe.expand.formula <- function( h,
        formulae,
        subjects = happy.get.subjects(h),
        add.THE.LOCUS      = FALSE,
        minus.THE.LOCUS    = FALSE,
        THE.LOCUS          = NULL,
        THE.LOCUS.model    = NULL,
        dmat.transform.FUN = NULL,
        verbose            = FALSE)
# Modelled after Richards private.substitute.locus() and parse.expanded.formula()
#    with differences:
#      1) an arbitrary number of formulae can be passed to the function
#      2) term expansion is bracketed where necessary, eg,
#        genotype:locus -> locus.<GENOTYPE.STRING>
#        additive:locus -> (locus.<STRAIN.1> + locus.<STRAIN.2> + ...)
# Difference (2) corrects a flaw in private.substitute.locus() and
# parse.expanded.formula() that resulted in incorrect expansion of
# interaction terms like,eg:
#     additive:locus * GENDER -> locus.<STRAIN.1> + ...
#            + ... locus.<STRAIN.N-1> + locus.<STRAIN.N> * GENDER
#
# General info:
#
# Takes a text string and expands references to genotypes etc in it.
# A reference of the form
# genotype:rs12341
# (where rs12341 is the name of a SNP) is found,
# then the genotypes for that SNP are loaded as a covariate factor
# and the formula modified to include the SNP.
# Note that the SNP name must only contain alphanumeric characters plus "-" or "_" or "."
# In particular is must not contain "+" or "*" or ":"
# If the SNP name is not a legal R variable name it is converted using make.names()

# Note that a reference to the genetic locus to be tested can be included as the string "THE.LOCUS"
# If no such reference is found then it is added
# This is not parsed in this function but is substituted later

# In this way it is possible to encode interactions between the locus and covariates
# e.g. GENDER*THE.LOCUS, GENDER:THE.LOCUS, genotype:rs3421 * THE.LOCUS
# or additive:rs234234
# or full:rs34234

# It is important that no covariate name is a superstring of "THE.LOCUS"
# It is important that spaces are included between the * and + in these models
{
  # check arguments
  if (is.null(formulae) | 0==length(formulae))
  {
      stop("Formulae vector must be non-null and be of non-zero length\n")
  }
  if (!is.formula(formulae))
  {
    for (i in 1:length(formulae))
    {
      if (is.formula(formulae[i])) next

      if (is.na(formulae[i]))
      {
          stop("Missing values in formulae[", i, "]\n")
      }
    }
  }
  formulae <- formula.as.string(formulae)
  if (0==length(subjects))
  {
    warning("No subjects requested in bagpipe.expand.formula()!\n")
  }
  subjects <- as.character(subjects)

  # incorporate THE.LOCUS
  if (any(minus.THE.LOCUS))
  {
    if (length(minus.THE.LOCUS)!=length(formulae))
    {
        minus.THE.LOCUS = rep(minus.THE.LOCUS,
                length.out=length(formulae))
    }
    formulae[minus.THE.LOCUS] <- drop.formula.vars(
            formulae[minus.THE.LOCUS], "THE.LOCUS")
  }
  if (any(add.THE.LOCUS))
  {
    i <- setdiff(which(add.THE.LOCUS), grep("THE.LOCUS", formulae))
    formulae[i] <- paste(formulae[i], " + THE.LOCUS", sep="")
  }
  if (!is.null(THE.LOCUS) & !is.null(THE.LOCUS.model))
  {
    THE.LOCUS.model <- rep(THE.LOCUS.model, length.out=length(formulae))
    for (i in 1:length(formulae))
    {
        if (is.na(THE.LOCUS.model[i])) next
        locus <- bagpipe.formula.encipher.locus(THE.LOCUS, THE.LOCUS.model[i])
        formulae[i] <- gsub("THE.LOCUS", locus, formulae[i])
    }
  }
  else if (!is.null(THE.LOCUS) & is.null(THE.LOCUS.model))
  {
    for (i in 1:length(formulae))
    {
        formulae[i] <- gsub("THE.LOCUS", THE.LOCUS, formulae[i])
    }
  }

  # missing data 
  ok.subjects <- happy.has.subjects(h, subjects)

  # expand formulae
  seen.predictors <- list()
	for (extractor in bagpipe.formula.extractor.lookup()$extractor)
	{
		for (i in 1:length(formulae))
		{
			form        <- formulae[i]
			deciphered  <- bagpipe.formula.decipher(form, extractor=extractor)
			new.formula <- ""

			for (k in 1:length(deciphered$chunks))
			{
				chunk <- deciphered$chunks[[k]]
				new.formula <- paste(new.formula, sep="", chunk$before)
				locus.name  <- chunk$locus.name
				if (is.null(locus.name))
				{
					break
				}

				# if the locus is abstract, ignore it and go to the next specified locus
				if ("THE.LOCUS"==locus.name)
        {
          new.formula <- paste(new.formula, sep="", chunk$locus.cipher)
          next
        }

        # if the locus has been seen before, substitute in the saved terms.string
        if (!is.null(seen.predictors[[chunk$predictor.tag, exact=TRUE]]))
        {
          new.formula <- paste(new.formula, sep=" ",
              seen.predictors[[chunk$predictor.tag, exact=TRUE]]$terms.string)
          next
        }

        # otherwise, create a terms.string and grab the necessary data
        data <- bagpipe.get.design(h,
            locus          = chunk$locus.name,
            subjects       = subjects[ok.subjects],
            extractor      = extractor,
            extractor.args = chunk$locus.args,
            extractor.tag  = bagpipe.formula.extractor.lookup(extractor)$tag)
        if (is.nullOrEmpty(data))
        {
          if (is.null(data))
          {
            browser()
            stop("Cannot find locus data for ", model,
                    ":",locus.name, " from formula ", form, "\n")
          }
          stop("Zero rows of locus data for requested ",
                length(subjects), " subjects\n")
        }
        if (!is.null(dmat.transform.FUN) & 1<ncol(data))
        # allow functions that modify the design matrix
        # but ensure that colnames are still unique
        {
          data <- as.data.frame(dmat.transform.FUN(data))
          if (0==length(grep(chunk$predictor.tag, colnames(data))))
          {
              colnames(data) <- paste(make.names(chunk$predictor.tag), sep=".",
                      colnames(data))
          }
        }
        # record how this locus should appear in the formula
        terms.string <- paste(colnames(data), collapse=" + ")
        if (1<ncol(data))
        {
            terms.string <- paste("(",terms.string,")",sep="")
        }
        new.formula <- paste(new.formula, sep=" ", terms.string)

        # cache
        seen.predictors[[chunk$predictor.tag]] <- list(
          terms.string = terms.string,
          data         = data)
      }
      formulae[i] <- paste(new.formula, sep="", deciphered$tail)
		}
	}

  # cbind any locus data together
  data <- NULL
  if (0!=length(seen.predictors))
  {
    for (i in 1:length(seen.predictors))
    {
      if (is.null(data))
      {
        data <- seen.predictors[[i]]$data
      }
      else
      {
	      # avoid using data frames to allow duplicate row.names
        data <- cbind(data, seen.predictors[[i]]$data)
      }
    }
  }

  # pad out locus data with NA rows if necessary
  if (!all(ok.subjects) & !is.null(data))
  {
  	unpadded.data <- data
  	data <- matrix(
  	    nrow=length(ok.subjects),
  	    ncol=ncol(unpadded.data),
  	    dimnames=list(NULL, colnames(unpadded.data)))
  	data <- as.data.frame(data) # this may need to be omitted when ncol=1
  	data[which(ok.subjects),] <- unpadded.data
  }    
  list(formulae=formulae, locus.data=data)
}

bagpipe.parse.sdp.string <- function(h, sdp.string)
{
	SPLIT.TOKEN="."
	num.founders=happy.num.strains(h)
	token=ifow(igrep(pattern=SPLIT.TOKEN, sdp.string, fixed=TRUE), SPLIT.TOKEN, "")
	sdp.symbols=unlist(strsplit(sdp.string, split=token, fixed=TRUE))
	
	if(length(sdp.symbols)!=num.founders)
	{
		bagpipe.formula.error("SDP must specify grouping for ", num.founders, " founders, but '",sdp.string,"' implies ", length(sdp.symbols), " founders")
	}
	sdp.symbols
}

