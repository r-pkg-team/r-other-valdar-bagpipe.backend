bagpipe.data.error <- function(...){
  stop("Bagpipe Data Error: ", ..., "\n", call.=FALSE)
}

bagpipe.proc.message <- function(...){
	cat(..., "\n")
}

bagpipe.input.error <- function(...){
  stop("Bagpipe Input Error: ", ..., "\n", call.=FALSE)
}


